---
title: "Well-Shaped Words"
description: "Great documentation for your software project"
date: 2022-01-13T15:21:19-03:00
tags: []
toc: true
draft: false
---
<p class=slogan>
<em>Great documentation for your software project.</em>
</p>

Hello! I'm Matt Dodson, a freelance technical writer and editor.
I specializing in writing for developer audiences&mdash;both experts and beginners.
In the past I worked at Grafana Labs, writing technical docs and content for the [k6](https://k6.io/docs) team.

My work includes API documentation, software guides, long-form articles, and technical books.
If interested, you can check out my [portfolio](./work/portfolio), [services](./work/services), and [philosophy of technical writing](./essentials).

I like Linux, languages, and literature.
I'd like to combine these three Ls someday.
But my current work is pretty cool, too.

I prefer free, open, and copyleft.

{{< note >}}
**I believe the web is for everyone**.

If this site has an accessibility problem, let me know, and I'll do my best to fix it.
{{< /note >}}

