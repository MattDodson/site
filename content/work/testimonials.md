---
title: "Testimonials"
description: "What others say about my work."
lead: ""
date: 2021-12-31T22:06:34-03:00
tags: []
toc: false
draft: false
---


{{< blockquote author="Pawel Suwala, CTO of k6, Grafana Labs" >}}
Working with Matt is always a seamless and efficient experience. His consistently high-quality work quickly earned the trust of the engineers on our team to the point where double-checking is unnecessary.

One aspect that truly stands out about Matt is his dedication to mentoring others in their writing skills. His extensive knowledge of the subject improved writing skills of many engineers around him.
{{< /blockquote >}}

{{< blockquote >}}
Matt did a excellent work with very limited guidance. It is hard to find a technical writer who has such a good understanding of software engineering principles and programming.
{{< /blockquote >}}


{{< blockquote >}}
Matt is an incredible hire that's flexible and accommodating to your company's needs.

We hired Matt as a technical writer to help us revamp our documentation. However, the scope of the project kept increasing and Matt was amazingly adaptable to the changes that were constantly occurring at our fast growing startup. He helped us revise existing articles, create a new operating guide, redid our API docs, and more. He is truly a wonderful addition to the team and we couldn't have done it without him!
{{< /blockquote >}}

{{< blockquote >}}
Matt is absolutely THE best. Very much a trusted advisor to me at this point. His comments and thoughts are always spot on. And he focuses on exactly what I asked him to focus on -- the content of the help file I asked him to build.

The project started out very small, but it grew into many wonderful conversations about the whole project of documenting a complicated, confusing piece of software. I highly recommend Matt for nearly anything, but for help with technical writing / documentation in particular.
{{< /blockquote >}}
