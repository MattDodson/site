---
title: "Portfolio"
description: >-
  A list of technical writing projects, starting from the latest thing I worked on.
  Where I have permission, I link to the docs that I helped create.
lead: >- 
  This list starts from the latest thing I worked on.
  Where I have permission, I link to the docs that I helped create.
date: 2021-12-31T23:52:55-03:00
lastmod: 2021-01-26T14:00:00-03:00
tags: []
toc: false
draft: false
---

If you want to see some representative, larger-scale projects, I recommend looking at my work summaries for:

<dl>
<dt><a href="#k6-and-grafana-labs">k6 and Grafana labs</a></dt>
<dd>Open source applications for mission-critical web infrastructure</dd>
<dt><a href="#unit21">Unit21</a></dt>
<dd>An enterprise web application, with an API and knowledge base</dd>
<dt><a href="#trueblocks">TrueBlocks</dt>
<dd>An open-source application, with a command line and API</dd>
<dt><a href="#ivelum">ivelum</dt></a>
<dd>A web development company, with enterprise and open-source projects</dd>
</dl></br>

There are a few projects that I had strong confidentiality agreements about.
I don't mention those.
Everything else is in the open!

{{< note >}}
To automate formatting, I built this page using a YAML file and Hugo's template language.

If you want to see how I did it, see
[this page's source file](https://gitlab.com/MattDodson/site/-/blob/main/content/work/portfolio.md),
[the `< jobs >` shortcode that it uses](https://gitlab.com/MattDodson/site/-/blob/main/layouts/shortcodes/jobs.html),
and [the YAML data file](https://gitlab.com/MattDodson/site/-/blob/main/data/jobs.yaml).
{{< /note >}}

{{< jobs >}}

