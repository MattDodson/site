---
title: "Services"
description: "I am not freelancing now, but here's how I used to work."
lead: "Docs for you, money for me."
date: 2021-12-31T23:58:08-03:00
tags: []
toc: true
draft: false
---

## What I do

The work I do has a few broad categories:

-   Writing documentation for developer audiences─API references and guides
-   Building and deploying docs site
-   Technical editing on all levels, grammar and architecture
-   Consulting about information architecture and technical writing
-   Teaching English as a second language

Usually, the projects I work on involve a bit of everything, though teaching
is different enough that [I describe it in its own section](#teach).

## How I work

People like my work.
Many of my clients return for more help. Some have offered me full-time positions.

Most importantly, I try to be easy to work with and reliable.
Here are some other principles I try to follow:
{{< ticks >}}
* Don't be afraid to ask a stupid question.
* Try to figure things out&mdash;first as a user, then as a code and chat archeologist
* Make something useful quickly, then iterate.
* Look for good spots to reuse or automate content.
* Strongly encourage user testing.
* Always give an honest opinion, but don't be dogmatic.
* Leave something that the client can continue to develop for years.
{{< /ticks >}}

## What I teach { #teach }

The things I can teach are:

-   Technical-writing tutoring or consultation, on the individual or organizational level
-   English as a second language. One-time, one-hour conversation, with a follow-up report.
This report describes the errors you made, vocabulary that came up, and provides some advice with ways to improve fluency, grammar, and pronunciation.
It is (almost) guaranteed to help you. 

I'm an experienced ESL teacher.
I've taught in schools, businesses, and online.
I consider technical writing an extension of teaching.
Feel free to [see my old Italki profile](https://www.italki.com/teacher/5516413), which has lots of student reviews from 2000+ classes. 

(In case Italki disables the account for being inactive, [I've made an archive copy](https://web.archive.org/web/20211217121748/https://www.italki.com/teacher/5516413).)

## Get in touch { #get-in-touch }

If you are interested in working with me, send me an email: `{{< email >}}`.

To speed things up, please provide a short description of what you want to do.
If it's possible, I'd also appreciate links to the project or the code base. 

After I have an idea of the work, I can provide a quote.
I work only by the hour, unless the offered whole-project rate seems very generous.

## Technology that interests me

The main things I'm interested in are Linux and language-and-text processing (from NLP to simple regex).
I'm also interested in general problems of design and of information organization.
I can program a bit in JavaScript, Bash, and Python.
I'm literate in a few more languages.
I know some SQL.

For presenting sites, I'm a big fan of using a static-site generator with Git.
For SSGs, I have experience with Jekyll, Docusaurus, Antora, and Hugo.

If your project involves one of the above, great!
But if not, I always like to learn something new. 

For an overview of authoring techology I've used, refer to my [portfolio]({{< relref "./portfolio" >}}).

