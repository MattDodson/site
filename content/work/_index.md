---
title: "Work with me"
author: ["Matt Dodson"]
draft: false
description: "Info about working with technical writer Matt Dodson."
date: 2021-12-31T16:56:08-03:00
tags: ["about"]
---

I like working at all stages of a project: finished, ongoing, upcoming, and even speculative.

Sometimes you might have an idea and just want to think about how you _would_ present it.
I can help with that too.
