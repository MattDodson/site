---
title: |
  Sentence repair: technical edits for two academic texts
description: |
  Applying the practices of a technical editor to two verbose texts found in the wild.
lead: |
  Applying the practices of a technical editor to two verbose texts found in the wild.
date: 2023-06-17T23:15:24-03:00
tags: ["editing"]
keywords: []
toc: true
draft: false
---

I approach editing with the same creed as a doctor: [_first do no harm_](https://en.wikipedia.org/wiki/Primum_non_nocere).
My constant doubt is I'll over-edit and change the meaning.
This is why I have researched set of [practices]({{< relref "../essentials/practices" >}}): to reduce guessing and have a standardized set of quality improvements.

However, when reading for fun, I sometimes can't stop myself from trying to fix sentences that confuse me.
Here are two examples of how writer of software documentation would fix two texts that have nothing to do with software.

## Wait, who did Spinoza write for?

This little primer on Spinoza is clear and well-written, but this particular sentence is hard to understand.

{{% blockquote %}}
It may surprise you to hear that Spinoza’s _Ethics_ was written just as
much for a non-expert audience in the twenty-first century as for the
philosophical world of the seventeenth.

---Beth Lord, _Spinoza's Ethics_
{{% /blockquote %}}

It's hard to diagnose exactly why:
at 33 words, it's a bit long, and the negation, "non-expert" is avoidable.
But no major flaw jumps out.
Yet it's hard to understand the sentence's relationships.

Having read this sentence a few times, I take it to literally mean that Spinoza wrote this book for two audiences:
- The philosophical world of the 17th century
- Non-experts of the 21st century.

The surrounding context, though, doesn't give Spinoza so much foresight.
Rather, Lord wants to say that the audience of this book have always been experts, "steeped in the philosophical tradition."
I think the confusion comes from the length and the long phrases.

How about one of these? One focuses on the difficulty of reading it, and another focuses on the publication audience.

To emphasize the difficulty of the text:

  > It may surprise you to hear that Spinoza's _Ethics_ has always been for a specialist audience. A non-expert of the seventeenth century would find the book just as difficult to read as a non-expert of the twenty-first century would.

To emphasize who the work was published for:

  > It may surprise you to hear that Spinoza's Ethics were written for an expert audience.
  > This was true when it was published for the philosophical world of the seventeenth century, and it's just as true today.

This is an example where expanding might make a sentence easier to parse.
In the next example, cutting text makes the sentence clearer.

## Oracular or plain verbose?

This example is trickier.
Though shorter, it has many more phrases that I try to avoid.
Yet I think fans of this work would find my proposed edits disagreeable.

{{% blockquote %}}
A building or town will only be alive to the extent that it is governed by the timeless way.

---Christopher Alexander, _The Timeless Way of Building_
{{% /blockquote %}}

First, a note on style:
from the little I've read, Christopher Alexander seems to like these orbicular ways of writing, and perhaps some readers find this style imbues a "timelessness" of its own.
Furthermore, the passive construction arranges the sentence to end with _timeless way_, which perhaps reinforces his central subject.

In an essay, such rhetorical flair is the author's prerogative.
But in a piece of technical writing, I'd chop this down.
Though the sentence is short, it could be shorter:

- _Will only be alive._ Why use the future? Was this false at the time of writing?
- _Be alive._ Why use an adjective when a verb exists?
- _Only be_. What is _only_ modifying? The verb _be_ or the extent?
- _To the extent that._ Doesn't _only_ imply _to the extent_?
- _Is governed by._ Is the passive functionally necessary?

Let's chop it down progressively.
I'll let you decide where I went too far.

First, removing the passive and future doesn't change meaning at all, but it brings the character count from 93 to 87.

> A building or town is only alive to the extent that it is governed by the timeless way. 

Removing the passive gets it down to 82.

> A building or town is only alive to the extent that the timeless way governs it. 

Next, does the distinction between "it is alive" and "it lives" matter here?
If not, we can get the character count down to 78.

> A building or town lives only to the extent that the timeless way governs it.

Aren't "only" and "to the extent" redundant? 
Maybe only acts as necessary emphasis.
But if not, removing only gets it to 73.

> A building or town lives to the extent that the timeless way governs it.

Or maybe we could just compress it in a relative clause.
Even restoring the passive, this version brings the character count down to 64. 

> A building or town lives only when governed by the timeless way.

It seems like Alexander wants to make a boundary in time and space delimited by the "the timeless way": life and timeless governance on one side, death and ephemeral governance on the other.
Perhaps his "to the extent that" conveys this boundary better than my truncated version does. 

Have I changed the essential meaning too much?

## A note on style and audience

As an editor, I'd be much more comfortable proposing my change to Beth Lord's text than I would to Christopher Alexander's.
Lord's book is about philosphy, but her goals are much closer to those of a technical writer: make a difficult topic easier to approach.
Alexander's work is in some sense a philosophy of its own, which specifically mimics works like the _Tao Te Ching_.

In any case, I'm not being totally fair to either author, because they have different audiences and style considerations than I do.
People rarely settle in to enjoy a technical text as they would for a favorite book.

The audience of my technical texts are usually reading to accomplish something else, and a large majority of them may be reading in a second language and under stressful work environments.
For this reason, I impose a minimalist, even brutalist, style on my technical texts, a style that would ruin the work of many great prose writers.
As a technical editor, I would not be able to ethically operate on the works of, for example, Annie Dillard or Sir Thomas Browne. 

