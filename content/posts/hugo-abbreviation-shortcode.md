---
title: |
  A shortcode for dynamic abbreviations
description: |
  The first call prints the whole name with the abbreviation in parenthesis.
  Subsequent calls print the abbreviation with a tooltip.
lead: |
  Hugo has a scratchpad to change a page's state.
  This shortcode uses the scratchpad to conditionally format an abbreviation:
  1. On first use, write the whole name and give its abbreviation in parenthesis.
  1. On subsequent uses, write the abbreviation and provide the whole name when the cursor hovers over the word.

date: 2023-07-03T10:06:11-03:00
tags: ['hugo', 'docOps']
toc: true
draft: false
og_image: |
  /images/og/abbreviation_shortcode_wellshapedwords.png
lol:
  term: Mean time to repair
  abbreviation: MTTR
---

{{< note >}}

**This shortcode is mainly for acronyms and initialisms.**

That is, abbreviations made from the first letters of the words in a phrase (like _NASA_ or _UDP_).
For details and usage advice, read the Google Style topic about [abbreviations](https://developers.google.com/style/abbreviations).

{{< /note >}}

This shortcode ensures that an abbreviation is always defined on a page and always has an HTTL `abbr` element.
It also saves writers from writing verbose, error-prone HTML and needing to track when a term first appears on a page.

For example, this text uses a shortcode:

```html
_{{</* abbr "mttr" */>}}_ is a basic measure
of maintainability in repairable systems.
If two systems have equal types
of failure and equal failure rates,
the system with the lower {{</* abbr "mttr" */>}}
has higher availability.
Let's say it one more time for no reason:
{{</* abbr "mttr" */>}}.
```

On the page, it renders as follows. Notice the abbreviations have tooltips:

> _{{< abbr "mttr" >}}_ is a basic measure of maintainability in repairable systems.
> If two systems have equal types of failure and equal failure rates, the system with the lower {{< abbr "mttr" >}} has higher availability.
> Let's say it one more time for no reason: {{< abbr "mttr" >}}.

Abbreviations should generally be avoided.
But when they are necessary, this shortcode improves the experience for both readers and writers.

## Hugo requirements

The following examples are ready to copy and paste.
To use them, change the terms and abbreviations for your lexis.

But, to modify these or make your own versions, it helps to know the following Hugo concepts:

{{< ticks >}}

- [Variables](https://gohugo.io/templates/introduction/#variables). Required both to make custom variables and to understand how to use shortcode, page, and [.Scratch](https://gohugo.io/functions/scratch/) variables.
- [Functions](https://gohugo.io/functions). Required to make templates to read data and conditionally write text.
- [Context](https://www.smashingmagazine.com/2021/02/context-variables-hugo-static-site-generator/). Required to understand how Hugo interprets a variable reference.

{{< /ticks >}}


## Steps to implement a conditional shortcode

The general procedure is as follows:

1. Define a term and its abbreviation. To facilitate reuse, store the data in variables or key-value pairs.

    ```html
    {{ $term := "Mozilla Developer Network" }}
    {{ $abbreviation := "MDN" }}
    ```


1. Use another variable to give each term a unique counter.

    ```html
    {{ $count_id := print ( $abbreviation | lower ) "_count"}}
    ```

1. In a shortcode, create a conditional statement based on the counter's value.
If the count has started, write the HTML abbreviation.

    ```html
    {{ if gt ( .Page.Scratch.Get $count_id) 0 }}
      <abbr title="{{- $term -}}">{{- $abbreviation -}}</abbr>
    {{ .Scratch.Get $count_id }}
    ```

1. In the `else` block, write the full phrase with its abbreviation. Then use the scratch pad to set the counter to `1`.

    ```html
    {{ else }}
      {{ $term }} ({{ $abbreviation }})
      {{ .Page.Scratch.Set ($count_id) 1 }}
    {{ end }}
    ```

I combined these snippets in a shortcode located at `layouts/shortcodes/abbreviations/mdn.html`.
I can call it as follows:

```html
> Do you know the {{</* abbreviations/mdn */>}} docs?
>
> Yes! I love the {{</* abbreviations/mdn */>}} docs.
```

It renders as follows:

> Do you know the {{< abbreviations/mdn >}} docs?
>
> Yes! I love the {{< abbreviations/mdn >}} docs.

However, if you have multiple terms, this shortcode has avoidable boilerplate.
**The following sections describe some better, more general solutions.**

## Design decisions (and examples)

Hugo is flexible about how it handles variables and logic, so you have many ways to implement this shortcode.
The following sections outline some approaches to take.
When possible, **I recommend keeping terms in a separate data file.**

### Keep terms in a data file

One method is to use a data file to store your terms in an array.
I like this approach because it separates concerns and because the list can be reused as the source of other templated pages (like glossaries).

1. Write your abbreviations in `data/terms.yaml`:

    ```yaml
    - term: Transmission Control Protocol
      abbreviation: TCP
    - term: User Datagram Protocol
      abbreviation: UDP
    ```

2. Range over the abbreviations in a shortcode.

    ```html

    <!--- variables based on shortcode argument -->
    {{ $entry := (.Get 0 | lower) }}
    {{ $count_id := print $entry "_count"}}

    <!-- range over data file -->
    {{ range $.Site.Data.terms }}
       <!-- Find matches -->
        {{ if eq  ( .abbreviation | lower) $entry }}
            {{ if gt ( $.Page.Scratch.Get ( $count_id )) 0 }}
                <abbr title="{{- .term -}}">{{- .abbreviation -}}</abbr>
            {{ else }}
                {{ .term }} ({{ .abbreviation }})
                {{ $.Page.Scratch.Set ( $count_id ) 1 }}
            {{ end }}
        {{ end }}
    {{ end }}
    ```

3. Call it using the abbreviation as an argument.

   ```html
   The {{</* abbreviations "tcp" */>}} is not the
   {{</* abbreviations "udp" */>}}.
   Again,
   {{</* abbreviations "tcp" */>}} is not
   {{</* abbreviations "udp" */>}}.

   ```
   This renders as:

   > The {{< abbreviations/main "tcp" >}} is not the
   > {{< abbreviations/main "udp" >}}.
   > Again,
   > {{< abbreviations/main "tcp" >}} is not
   > {{< abbreviations/main "udp" >}}.

The drawback is that it requires a little more conditional coding, and the syntax to call it is a bit more complex.

### A shortcode for each file

If you want each acronym to use its own file or shortcode call, try this approach:


1. In a partial, write the reusable logic. This file is called `abbreviation.html`:

    ```html
     {{ $count_id := print ( .acronym | lower ) "_count"}}

     {{ if gt ( .context.Scratch.Get $count_id) 0 }}
        <abbr title="{{ .term }}">{{- .acronym -}}</abbr>
       {{ .Scratch.Get $count_id }}
      {{ else }}
        {{ .term }} ({{ .acronym }})
        {{ .context.Scratch.Set ($count_id) 1 }}
    {{ end }}
    ```
1. In a shortcode, define the term and abbreviation, and then call the partial.

    ```html
     {{ $term := "transmission control protocol" }}
     {{ $abbreviation := "tcp" }}

     <!-- reusable boilerplate -->
     {{ partial "abbreviation.html"  (dict "term" $term "abbreviation" $abbreviation "context" .page ) }}
    ```

The call syntax is slightly simpler, but I don't like much else about it.
Still, I'm happy I figured out how to do this because now I know how to pass variables and context to a partial (thanks to [help from the Hugo forums](https://discourse.gohugo.io/t/is-it-possible-to-reuse-logic-and-variables-between-shortcodes/45042)).

### In-line

If you plan to use the shortcode exactly once, you can use an [inline shortcode]({{< relref "hugo-inline-shortcodes" >}}) and define terms in the page parameters.
Setting terms in the page parameters probably has a number of reasonable uses,
but
I can imagine only two times where an inline shortcode makes sense for this use case:
- If you are writing a long blog with acronyms that you never plan to use again.
- If you are writing in a modularized set-up where it's difficult to change the `layouts` or `data` directories.

Nevertheless, if you want to do this, read the [inline example]({{< relref "hugo-inline-shortcodes#synthesis">}}).

## Discussion and links

This shortcode is handy for docs where abbreviations for specialized terms are often used.
Perhaps you could combine it with a prose linter to enforce formatting.

On broader note, this shortcode hints at something more powerful: a way to conditionally format text based on a mutable page state.
Now that I've figured out these small examples, maybe I'll discover some greater use of the Hugo scratch pad.

For more reading, these helped me make this post.
- Blog posts about Hugo `.Scratch`
    - [Hugo Scratch explained](https://www.regisphilibert.com/blog/2017/04/hugo-scratch-explained-variable/)
    - [Hugo scope in a template](https://code.luasoftware.com/tutorials/hugo/hugo-scope-variable-in-template/)
- The forum [post I made](https://discourse.gohugo.io/t/is-it-possible-to-reuse-logic-and-variables-between-shortcodes/45042) about how to share logic and variables between shortcodes.
