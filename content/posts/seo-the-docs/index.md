---
title:  |
  SEO basics are all you need: too much "optimization" worsens docs
description: >-
  Following the basic SEO guidelines probably improves document design. But there's a limit. Humans will struggle to read a site that's over-optimized for search engines.
lead: >-
  Following the basic SEO guidelines improves document design. But there's a limit. Humans will struggle to read a site that's over-optimized for search engines.
date: 2022-03-07T07:59:32-03:00
tags: ["seo", "documentation"]
toc: true
draft: false
---

For a long time, I knew nothing about search-engine optimization.
I avoided learning about it.
I thought that SEO was something for marketers and other mercantile types,
not for technical-writing "purists" like me.

Of course, this was a silly line of thinking:
technical writers should about SEO.
Why wouldn't I want to make my docs findable?
Everybody searches!

Besides, how could I dismiss SEO if I didn't know how it worked?
Now that I've learned a bit, I can say some SEO is a good thing, but too much will harm your docs.
<!--more-->

## Standard disclaimer: I'm ignorant

I'm far from an SEO expert.
I've read two books[^1] and the [Google search essentials](https://developers.google.com/search/docs/essentials).

Besides, I believe that in the long run, _making any page "perfect" for search engines is a doomed strategy._
What is optimal today may be suboptimal tomorrow.
I explain this a bit more at the end of this article.

## The good parts

It's very important to support searching.
Even if you have no commercial interest, don't you want people to find what you write?
Fortunately, the official recommendations
of SEO are perfectly harmonious with the guidelines of good doc writing.


### SEO essentials are doc essentials

The following guidelines are just good practices for writing:

{{< ticks >}}
* Have descriptive headers.
* Make accessible pages.
* Break up your content and make it scannable.
* Use descriptive link text.
* Promote docs in other channels (e.g. on Twitter)
* Write good content that people want to link to.
* Write good content that people want to link to (I know I repeated myself).
{{< /ticks >}}

### Metadata is important too

Some of the more advanced concepts are good ideas for docs too.

* You can reuse a good meta `description` as a page summary, both for your site and social shares.[^2]
* If you already use [structured authoring](https://en.wikipedia.org/wiki/Structured_writing),
using Schema.org and JSON-LD might help you classify your content.
- You can use the sitemap to create visualizations of your information flow.

## The bad parts: trying to hack SEO

All documentation should be readable and findable.
A little SEO should help readers find your docs on search engines.
But if you blindly follow the latest recommendations, they'll struggle to actually read the text.

Here are some _2022 SEO best practices_&trade; that create bad docs.

### Dubious: intentionally create long pages

In SEO jargon, _dwell time_ is how long a user stays on a site.[^3]
The idea is that, if a user stays on a page for a long time, then that page is probably useful and interesting.
In practice, people try to increase dwell time by padding out content.

Perhaps that makes sense for blog content, but "lengthy" is pretty much an anti-goal for documentation writers.
I don't want readers to stay on my documentation site for a long time.
I want them to get in, find what they need, leave quickly, and get on with their lives.

Fortunately, I don't think many documentarians are trying to make arbitrarily long docs.
But many web-content writers are.
In fact, I would say that the goal of "dwell time" has created far more poor, bloated content than useful longform content.

1. Use Google to search for a recipe of your favorite food.
2. Click any result in the first page.

Is the page longer than a recipe in a book?
Are the actual ingredients and procedures buried two-thirds of the way down the page?

If there's a topic that's worthy of more than a few short paragraphs, then the writer's big challenge is figuring out what information to leave out, not how to cram more in.

### Dubious: use a lot of synonyms


Some SEO guidelines I've looked at say to sprinkle synonymous and related terms throughout your content to help the  _latent semantic indices(LSIs)_.
An LSI is essentially a thesaurus.
It helps search engines return results for "on-premises" when you search for "self-hosted".[^4]

Technical communication should use language that is precise and unambiguous.
Ideally, one term to match to one thing.
Terminology is a hard thing to nail down, and coherent documentation should use consistent names.

However, it does make sense to incorporate some synonyms, precisely to support searching.
* In introductory sentences that start like "In some places, this term is also called..."
* In some kind of thesaurus or [other controlled vocabulary](https://www.oreilly.com/library/view/information-architecture-4th/9781491913529/ch10.html).

In other places, a document that uses different terms for the same thing is just confusing.
In a world where "namespace" already means so many different things, why increase ambiguity?

## Be wary of for-profit SEO advice

SEO is an arms race.
Google tries to figure out what makes good content; marketers figure that out and exploit it; Google penalizes overuse of the metric and comes up with something else.
If you try to be "optimal," you might end up with an unreadable site that has bad SEO anyway.
It doesn't help that ninety-five percent of the SEO advice that you'll find is either spam or a scam.

<figure>
  <img src="/posts/seo-the-docs/spam-scam-continuum.svg" alt="A hand-drawn diagram">
  <figcaption>Ninety-five percent of SEO content falls within the spam-scam continuum.</figcaption>
</figure>

A lot of these SEO "best practice" sites don't even know what they are talking about.
If they did, they'd probably keep their knowledge hidden.
Even Google engineers use machine learning to support results,
which means there's always some part of the rankings that is determined within a black box.

## Good SEO and Good docs

As I work on larger sites, I've come to appreciate how important it is to support searching.

However, for your docs, don't worry too much about SEO.
Instead, make useful, accessible content that people want to link to.

For SEO, I also have a great long-term strategy.
Make useful, accessible content that people want to link to.


[^1]: _SEO in 2022_, by Adam Clarke, and _Ultimate Guide to Link Building_, by Garrett French and Eric Ward.
[^2]: [This site uses the description as a top-level summary](https://gitlab.com/MattDodson/site/-/commit/48fbcb3f414f937905d27f5b6a9ba704d48b2177).
[^3]: I've also seen it called "stay time" and "retention".
[^4]: Google apparently doesn't use LSI, but something more advanced https://backlinko.com/hub/seo/lsi
[^5]: https://www.seroundtable.com/google-bold-seo-32418.html

