---
title: "Automate link checking"
description: >-
  A (perhaps ongoing) series of topics about link checking.
lead: >-
  A (perhaps ongoing) series of topics about link checking.
date: 2022-02-10T08:50:59-03:00
tags: ["docOps", "linkchecking"]
toc: false
draft: false
---

On a text-heavy site, the hyperlink is the main element that the reader interacts with.
So, a broken link breaks the essential interface.

And links break all the time.
The most reliable way to deal with this is to automate link-checking.
