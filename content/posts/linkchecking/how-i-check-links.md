---
title: "How I automatically link check my Hugo site"
description: "I use a linkchecker in Gitlab's CI"
lead: "Every time I commit, a program in my CI checks for broken links."
date: 2022-02-15T22:11:01-03:00
weight: 198
tags: ["linkinator", "Docops", "hugo", "documentation", "linkchecking"]
keywords: ["How to use linkinator to check broken links", "how to check broken hugo links in gitlab CI"]
toc: true
draft: false
---

This is going to be a short topic, because my link-checking method is simple.
In my Gitlab repo, I have a continuous integration(CI) script that:

1.  Builds the Hugo site. If something fails, it stops.
2.  Checks the newly built site for broken links. If any link breaks, it stops.
3.  Deploys the site to my server.

This is not a particularly robust way to check links, [for the reasons I explained in my "Link check your site post"](/posts/linkchecking/link-check-your-site#tradeoffs).
But it's good enough for now it's good enough.

## Required software {#required-software}

You'll need:

-   A CI server. I use GitLab, but I've done this in Github too.
-   A link checker that your CI can download.

For the link checker, I use [Linkinator](https://github.com/JustinBeckwith/linkinator-action), a TypeScript application.
I don't remember how I started using it, but I keep using it because it's fast enough for me, simple to use, and reliable.

The important thing is that your link checker can search *recursively*.
That is, for every internal link, you want the link checker to visit that page and check its links too.
It doesn't need to recursively check external links; if it did, it might check the whole internet.


## Steps to check links on commits to Gitlab {#steps-to-check-links-on-commits-to-gitlab}

Here's how I do it on my site:

1.  Have a Hugo site.
1.  Host it on Gitlab.
1.  In the root of your site's directory, create a file called
    `.gitlab-ci.yml`.

    The following steps all require you to edit this YAML file.

1.  In the YAML file, declare a base image. Install `npm` and `hugo` .

    My CI's image already comes with Hugo installed.
    It's an Alpine Linux image, so I'll use Alpine's `apk` package manager to install the software.

1.  Use `npm` to install linkinator.

1.  Build your Hugo site. Run linkinator recursively on the public folder.

1.  Test that it works. Intentionally add a broken link, and push it to the repo.

    If the link checker works, you won't deploy any more broken links, even if you push them.
    This will catch newly added links that are improperly formatted, and old links that have sadly rotted.

### Example CI script


Here's a slightly simplified version of what my CI YAML file looks like:
   ```yaml
   image: registry.gitlab.com/pages/hugo:latest

   variables:
     GIT\_SUBMODULE\_STRATEGY: recursive

   deploy:
     timeout: 5 minutes
     script:

      - apk add npm
      - hugo --minify
      - npm install -g linkinator
      - linkinator --recurse public/
   ```

   You can see the [full file, with deploy, in my site's repo](https://gitlab.com/MattDodson/site/-/commit/908c3bad66bca17e119d7e030243068ed1426af2).
