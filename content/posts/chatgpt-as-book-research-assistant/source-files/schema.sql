
CREATE TYPE age AS ENUM ( 'aristocratic', 'theocratic', 'democratic', 'chaotic' );
CREATE TYPE fenum AS ENUM ( 'Drama', 'Novel', 'Story collection', 'Philosophy/essay', 'Poetry', 'Memoir/Autobiography' );
CREATE TYPE form AS ENUM ( 'Drama', 'Novel', 'Story collection', 'Philosophy/essay', 'Poetry', 'Memoire/Autobiography' );
CREATE TYPE form_enum AS ENUM ( 'Drama', 'Novel', 'Story collection', 'Philosophy/essay', 'Poetry', 'Memoire/Autobiography' );
CREATE TYPE genre AS ENUM ( 'Drama', 'Novel', 'Story collection', 'Philosophy/essay', 'Poetry', 'Memoire/Autobiography' );


CREATE TABLE book(Author VARCHAR, Title VARCHAR, Form ENUM('Drama', 'Novel', 'Story collection', 'Philosophy/essay', 'Poetry', 'Memoir/Autobiography'), Genre VARCHAR, "Year" SMALLINT, Country VARCHAR, Age ENUM('aristocratic', 'theocratic', 'democratic', 'chaotic'));




