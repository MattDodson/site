

The `theocratic.csv` files and so on are generated source files.

`Book.csv` is the cleaned and compiled table from the four source files.
`Sample.csv` is tested sample, with comments.
These two files are the tables of the database.

To use with DuckDB CLI, run:

```
IMPORT DATABASE '.';
```
