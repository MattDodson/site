---
title: "How to use shortcode headings in the Hugo TOC"
description: |
    "Try using markdown headings in the shortcode and call the shortcode with the percent-sign delimiter."
lead: |
    "TL:DR: Try using markdown headings in the shortcode and call the shortcode with the percent-sign delimiter. I'm not sure why this works."
date: 2023-04-20T00:00:00
tags: ["hugo", "docOps" ]
keywords: [ ]
toc: true
draft: false
---

When I write headings, I always consider how they'll look in the auto-generated table of contents (TOC).
If the headings are descriptive,
the TOC creates a powerful navigation tool, providing:
- A high-level summary of the overall document
- A set of entry points to help readers find the sections that most relate to them.

I wrote a long post, [Most common edits in technical documentation]({{% relref "../../essentials/practices" %}}).
Because it was so long and regularly structured, I wrote a shortcode to template the content from different data files.
When my shortcode content didn't appear in my TOC for this on this Hugo site, I was sad.

But I accidentally found a solution: write the shortcode in markdown.


## Solution: markdown headings

In short:
- Use markdown headings in the shortcode (that's `##` instead of `<h2>`).
- Delimit the shortcodes with the percent sign (that's `%` instead of `<`).

Shortcodes with the `%` delimiter render before the page.
I don't know why the headings wouldn't work with a percent delimiter and HTML
headings, but this is what I've found.
For a demonstration, read the next section.

## Live demonstration

{{< last-version >}}

I've written [two shortcodes](https://gitlab.com/MattDodson/site/-/tree/main/layouts/shortcodes/toc-demo "link to shortcode directory") with level-three headings. 
The only difference is heading style:
- The `html-headings` shortcode uses HTML.

  ```html
  <h3>HTML heading, {{ (.Get 0) }}-delimited shortcode</h3>
  ```
- The `markdown-headings` shortcode uses markdown.
 
  ```markdown
  ### Markdown heading, {{ (.Get 0) }}-delimited shortcode
  ```

Now, I'll put the following snippet directly in the file that you're reading.


```markdown

{{</* toc-demo/html-headings "bracket" */>}}

This heading doesn't appear in the TOC.

{{</* toc-demo/markdown-headings "bracket" */>}}

No heading is created because the angle bracket
causes the shortcode to be processed as HTML.

{{%/* toc-demo/html-headings "percent" */%}}

This heading doesn't appear in the TOC.

{{%/* toc-demo/markdown-headings "percent" */%}}

This heading _appears_ in TOC.
Scroll back up and check yourself! 

![Drawing of a confused Jackie Chan](/images/jackie-what.jpg)
```

Here it comes:

---

{{< toc-demo/html-headings "bracket" >}}

This heading doesn't appear in the TOC.

{{< toc-demo/markdown-headings "bracket" >}}

No heading is created because the angle bracket causes the shortcode to be processed as HTML.

{{% toc-demo/html-headings "percent" %}}

This heading doesn't appear in the TOC.

{{% toc-demo/markdown-headings "percent" %}}

This heading _appears_ in TOC. Scroll back up and check yourself! 

![Drawing of a confused Jackie Chan](/images/jackie-what.jpg)

---

## Discussion

I'm not sure why only Markdown headings work.
As far as I can gather, before v0.55, no shortcode content could appear in a TOC, because the markdown parser didn't process shortcodes in any way.
Somehow, though, my HTML headings still get skipped by the part of the application that generates the TOC.

EDIT: with a bit of looking, I believe some better solutions might be to use `unsafe = true` in the config, or perhaps, in more complex shortcodes, using `RawContent`. If I investigate more, I'll make a new post.

## Related links

This seems to be a common problem in Hugo.
Here are some relevant links and issues that I could find.

- Hugo 0.55 release notes: [Shortcodes revisted](https://github.com/gohugoio/hugo/releases/tag/v0.55.0)

- Pulls:
  - [2323](https://github.com/gohugoio/hugo/pull/2623): Handle TOC before handling shortcodes
  - [5702](https://github.com/gohugoio/hugo/pull/5702): Make page an interface

- Issues:
  - [6690](https://github.com/gohugoio/hugo/issues/6690): Shortcode way of including md cause headings missed in TOC

- Community posts. _I think better solutions are here!_
  - [Applying headings in shortcodes to TOC](https://discourse.gohugo.io/t/applying-headings-in-shortcodes-to-the-table-of-contents/32132)
  - [URL/TOC issues](https://discourse.gohugo.io/t/url-toc-issues-when-inserting-one-page-into-another/43077)

