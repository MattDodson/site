---
title: |
  "Allows You To" considered harmful
description: |
  The phrase has less verbose alternatives, and  all of these intermediary verbs can obscure technical meaning.
lead: |
  Most of the time, it just obscures meaning. Even when necessary, it has a shorter alternative.
date: 2023-06-21T22:11:19-03:00
tags: ["grammar", "editing"]
keywords: []
og_image: "/images/reduce-noise-boost-signal-wellshapedwords.png"
toc: true
draft: false
---

{{% blockquote %}}
The Forward feature allows you to forward messages...
{{% /blockquote %}}

This is one of the most common constructions in technical documents---and one of the least useful.
In fact, you can always avoid the phrase "`feature` allows you to `verb`" and your docs will be better off:

- The phrase often incorrectly implies an intermediary between a behavior and its result.
- Even when an intermediary is necessary, the semantic meaning of _allows_ is ambiguous.
- Even when the phrase is appropriate, it has a shorter alternative, _lets you_.

That's the gist. The following sections elaborate with examples, citations, and proposed alternatives.

## Reasons to avoid "allows you to"

In short, the phrase always creates inaccuracy, imprecision, or unnecessary text.

### It's often inaccurate

Along with similar constructions like _lets you_ or _enables you to_, _allows you to_ can create an unnecessary grammatical layer between a feature and its behavior. 
In linguistic terms, such verbs are examples of _catenative verbs_, meaning they subordinate other verbs in a chain.
More specifically, they are examples of _causatives_, verbs that "cause" some other action. [^cambridge]

But these constructions often describe behavior that has no secondary effect.
Let's return to this text, lifted from an old version of the WhatsApp documentation (now fixed): [^whatsapp]

{{% blockquote %}}

The Forward feature allows you to forward messages from a group or individual chat to another group or individual chat.

{{% /blockquote %}}

Is this true? Does the forward feature _allow_ me to forward messages, or _does_ it forward messages?
Why not describe behavior directly?

> The Forward feature forwards messages...

Maybe that example sounds awkwardly redundant, but unnecessary catenative verbs commonly intrude in all sorts of technical description.
For example, this is from the
{{< mdn.inline >}}<abbr title="Mozilla Developer Network">MDN</abbr>{{< /mdn.inline >}} JavaScript reference.

> `Window.cancelIdleCallback()`
> : Enables you to cancel a callback previously scheduled with `Window.requestIdleCallback`.

What does the method do: cancel callbacks or enable cancelling a callback elsewhere?
Though this example uses _enables_ instead of _allows_, the effect is the same: unnecessary words and increased ambiguity.

[^whatsapp]: At least, from the quoted text in this [Stack Exchange Post](https://ell.stackexchange.com/questions/236923/is-it-better-to-say-this-feature-allows-you-to-or-this-feature-lets-you),
I infer that the WhatsApp page used to say "allows you to."


[^cambridge]: Pullum et al. _The Cambridge Grammar of the English Language._ Chapter 14, "Non-finite and verbless clauses."

### Its meaning is often ambiguous

Sometimes a writer really does want to describe a feature as a means to some end.
In this case, a catenative verb might be appropriate.
For example, these are from the {{< mdn.inline />}} docs:

> Making a copy like this allows you to effectively use the request/response again while varying the init options

> The Twitter API allows you to do things like displaying your latest tweets on your website.

Both of these sentences describe one action (making a copy, calling the Twitter API) that can help users accomplish some later action (reusing, displaying).
So causatives seem appropriate.
But even in these cases, are phrases like _allows you to_ or _lets you_ really the best constructions?

If you look up _allow_ or _let_ in any dictionary, you'll find the first definition is always about giving permission. [^dictionary]
Isn't it patronizing and innaccurate to think of a software object as giving me, a human, permission to display tweets on a website?
(I could just take a screenshot and bypass the API).
Of course, _allows you to_ also has a more general meaning (I discuss that in the last section).
But in software docs, where many readers are reading in a foreign language or using a machine translator, the least ambiguous option is best.

###  "Lets you" is shorter

![Comparing a red signal against two gray blocks of noise. The volume of the noise is proportional to the amount of filler text.](/images/reduce-noise-boost-signal-wellshapedwords.svg)


Maybe you find my previous arguments unconvincing.
Besides, some software features really do configure permissions.
Either way, _lets you_ is still a better construction.
The reason is undeniable: it's shorter.

Good technical writing should be [as short as possible]({{% relref "../essentials/principles#short" %}}).
Semantically, the following sentences are essentially identical: [^dictionary]

```diff
- The `authenticate` endpoint allows you to view resources according to your credentials.
+ The `authenticate` endpoint lets you view resources according to your credentials.
```

But _lets you_ uses five fewer characters.
If you can transmit the same meaning with less text, you should: boost signal, reduce noise.

In a single sentence, such an improvement is small and rather inconsequential.
But small changes add up,
and technical communicators should try to standardize quality at all levels.

I am not alone in this idea: the Google Style Guide also recommends _lets_ over _allows_. [^google]


## Alternatives

{{% note %}}

These ideas---this whole post---were generated from a thread on the WriteTheDocs Slack channel.
I thank everyone for the robust discussion.

{{% /note %}}

Instead of _allows you to_ and other intermediaries, tailor your vocabulary and syntax [to the goals of your text](/essentials/practices/#structure-content-according-to-different-reader-needs).

### In task-based docs, front the action

In a doc that describes procedures, consider inverting your syntax.
That is, place the result before the command. For example, a doc called "How to use Nginx" could have a phrase like this.

  > To stop the server, run `nginx -s quit`.

Beginning with an [_infinitive of purpose_](https://en.wiktionary.org/wiki/infinitive_of_purpose "The use of the infinitive form of a verb in answer to the implied question, why?") helps readers scan for what they want to do (since readers scan in an [F-shaped pattern](/essentials/principles#no-reading)).

### In references, put behavior before context

Descriptions in references should be spartan.
For example, a doc called "Nginx CLI reference" might have something like this:

> `nginx -s quit`
> : Stops the server

If it's important to communicate goals, you could provide context in a second sentence. For example:

> The OCR API processes a batch of images and detects common visual features.
> These groupings can serve as search filters for a large image database.

### Use the imperative

This is often the simplest and most direct way to describe the purpose of a tool.
For example, the Whatsapp doc quoted at the beginning now reads as follows:

  > Use the Forward feature to forward messages from an individual or group chat to another individual or group chat.

Some writer at Facebook also didn't like _allows you to_!

## But who cares about such a common and tiny phrase?

True, this issue is small in the grand scheme of things.
Good docs won't be spoiled by an _allows you to_ ({{< mdn.inline />}} is proof).
But these details do matter if you think systematically about creating quality documentation.
If _allows you to_ creates an unnecessary intermediary between an actor and action, it also creates inaccuracy, and technical communicators should always [Try to tell the truth](/essentials/principles#truth). Even when it's appropriate, shorter alternatives exist, and technical texts should be as simple as possible and no simpler.

It's also true that the phrase is common.
When it comes to language in ordinary life, I'm a descriptivist.
And I must acknowledge that _allows you to_ is a perfectly common expression, [^youglish]
very often used without any sub-text about giving permission.
However, technical writing represents a highly constrained use of language, where brevity and clarity are more important than "naturalness."

Avoid _allows you to_. This isn't a big issue, but it's an easy one to fix, and better is better.


[^dictionary]: For example, the [Cambridge dictionary entry](https://dictionary.cambridge.org/dictionary/english/allow) for _allow_ says "To give permission." The entry for _let_ says "To allow."
[^google]: ["Instead, use _lets you_."](https://developers.google.com/style/word-list#allows-you-to)
[^youglish]: For informal evidence, the expression has over 16 thousand occurrences in the [Youglish corpus](https://youglish.com/pronounce/allows%20you%20to/english?).
