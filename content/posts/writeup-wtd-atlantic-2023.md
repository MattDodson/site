---
title: "Write up: WTD Atlantic, 2023"
description: >
  I didn't think a virtual conference could be so fun. My thoughts as a speaker and attendee.
lead: >-
  I didn't think a virtual conference could be so fun.
   - [Video](https://www.youtube.com/watch?v=m5OxGst2sks)
   - [Source code](https://github.com/MattDodsonEnglish/WTD-Atlantic-presentation-2023)
   - [Abstract](https://www.writethedocs.org/conf/atlantic/2023/speakers/#speaker-matt-dodson-graphs-not-trees-a-ground-up-approach-to-fixing-a-docs-site-matt-dodson)
date: 2023-09-27T09:03:38-03:00
tags: ["documentation"]
toc: true
draft: false
og_image: >-
  /images/og/wellshapedwords-trees-graphs.png
---

Earlier this month, I presented a talk at Write the Docs called "Graphs, not trees: a ground-up approach to fixing a docs site."


<figure>
<img src="/images/dawson-wsw-trees-graphs-sketch.jpg"
alt="A summary of my talk in sketch form" >
<figcaption>Thanks to <a href="https://dennissdawson.wixsite.com/mr--dawson/portfolio">Dennis Dawson</a> for this great sketch</figcaption>
</figure>


Before I write up my thoughts, let me thank some people:
- [Yesica Torrico](https://www.linkedin.com/in/yesica-t-uxui/) for being the design brain behind the visuals, style, and overall organization
- [Sviatoslav Abakumov](https://github.com/Perlence) for giving an incisive critique of a quite crappy first version.

I also thank the conference organizers, who did a great job curating talks and running a tight ship; the speakers, who all gave interesting talks; and of course the participants, who made this feel as interactive as an in-person conference.
Through the chat, I really felt that the audience was an active participant in each talk.

## Preparation

It started with an {{< abbr "RFP" >}}.
I realized how inchoate my idea was only when I wrote the {{< abbr "RFP" >}}.
Then the {{< abbr "RFP" >}} was accepted, and I thought _what the hell did I write_?

From here the process went through a few drafts, starting with a little storyboard and then an outlined script.
The content of the images changed the content of the speech;
for example, I originally used that man-donkey image only because it amused me, but after I referenced it a few more times, it became a through line of my whole narrative.

One month out, I recorded a loose, semi-improvised talk and requested feedback from my friend Sviatoslav.
He pointed out a number of dead ends and faulty arguments.
So I went back and wrote a script (ironically, now that I knew what I wanted to say, I used a trunk-and-branch approach to re-design my argument). Responding to this feedback made the presentation much tighter.

{{% note %}}
The more I read about architecture, the more I regretted my metaphor.
The example is worn-down (c.f. Jane Jacobs, Stewart Brand, James Scott, and so on).
And, for each place I showed, I found comments from former residents who had positive memories.
Now I think the subject is so complex that I should've chosen another angle.
{{% /note %}}

## Tools

Effective communication does the most with its medium.
Since the presentation was pre-recorded, my medium involved not only voice and image but also moving tracks that can be layered, edited, and re-watched.
My original dream had animation and multi-track audio and video,
all disciplines I know nothing about&mdash;even just making suitable audio was above me&mdash;and soon I hauled that dream away.
[^1]

I do think the [RevealJS](https://revealjs.com) medium gave me some flexibility to animate and make transitions that worked with the content.

To control the slides, I used [KDE connect](https://kdeconnect.kde.org/).

## Presentations

The best part of the presentation was the live chat.
I was pumped up!

All the talks were great. My only complaint is that there were too many good choices.

I also enjoyed the format of the parallel unconference sessions.
I liked to speedily cycle through the rooms, a unique benefit of virtual conferences.
The equivalent in real life would be to constantly barge into new rooms, listen for five seconds, leave, then return five minutes later to pop my head in for another five seconds.

## Lessons

I'd never done a talk like this before.
If I could do it over, I'd try to get a good recording environment before I did anything else.
I underestimated how hard it is to get good lighting and sound, and by the time I recorded, it was too late to change environments.
I also realized I had a lot of "ums" and "kind of, like, you know, a sort of...", all noise that obscures the signal.

More advice for my past self:
- Don't underestimate how hard it is to learn a new technology. RevealJS, audio, and video all presented new challenges (especially the latter two).
- Get feedback from multiple perspectives.
- Get feedback on an early, rough form. More polished versions might limit ideas.
- Rewrite and practice&mdash;but leave room for improvisation.
- Let the material dictate the story.
- Start with the story, then the images, then rework the story. Add all the little finishing details at the end. Words are way easier to change than images.

Pretty bland and generic, to be honest. I think I knew all this before I did the presentation, but now I _really know_.

## Wrap up

I had never attended a professional conference, and I wasn't expecting much from the virtual format.
But the process ended up being wonderfully interactive, and there were so many smart ideas.

This was probably the most fun moment of my technical-writing career.

[^1]: For the line "Google is not a tower, it's a subway", I spent hours [animating the Google logo to look like a train](https://raw.githubusercontent.com/MattDodsonEnglish/WTD-Atlantic-presentation-2023/d645bcc14664c16063d542ea6f713292f98a2c03/static/google-train.svg). After watching multiple times to understand what I was trying to represent, my designer colleague asked why I was showing a freight train when I'm talking about a subway.<figure> <img src="/images/google-subway.png" alt="horrible graphic design"> <figcaption>Besides looking terrible, this "Google subway train" would give everyone in the station carbon-monoxide poisoning.</figcaption> </figure>

