---
title: "Shell tools for writers"
description: A collection of small shell scripts to make technical writing easier
lead: Here's a running list of small scripts that have saved me from tedious technical-writing tasks.
date: 2025-03-02T16:11:17-03:00
tags: ["docOps"]
toc: true
og_image:
---


The command line is a text-based interface,
and writing is a text-based activity.
Shell tools, therefore, are writing tools.
Here are some examples that have served me.



{{< note >}}
All scripts assume GNU versions and the Bash shell.
{{< /note >}}


## Find and replace


Replace all instances of an old phrase within a set of MD files.
This works best on a Git repo so that you can review the diffs before committing any changes.

```bash
old_term="relref \"/"
new_term="ref \"/"
find . -name "*.md" -exec sed -i "s/$old_term/$new_term/g" {} +
```


## Count and sort frequency across files

Some changes are too contextual for global search and replace.
If the fixes are made per page and incrementally, you can maximize early impact by starting on the pages that require the most changes.

This `grep | sort | awk` pipeline:
1. Searches for a term.
1. Sorts the list by frequency.
1. Filters out files with 0 occurances.

```bash
grep "relref \"/" -irc content/ \ 
 | sort -t : -k 2 --reverse \
 | awk -F : '$2 != 0'
```

## Filter JSON array of links checked

The [Linkinator](https://github.com/JustinBeckwith/linkinator) JSON format outputs a status report of every link on a site.

Most links work and some of the broken ones stem from 500 or 429 errors, which don't reflect link quality.
So I can use `jq` to filter only the links that have a status of `404`


```shell
linkinator --recurse --format JSON \
    "https://docs.rhize.com" \
    | jq '.links[] | select(.status == 404)'
```

