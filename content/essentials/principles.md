---
title: "Principles of good technical writing"
description: |
  If you work from a set of principles, decisions about how to communicate become a lot easier to make.
lead: |
  If you work from a set of principles, it becomes a lot easier to make decisions about how to communicate something.
date: 2023-01-04T18:18:06-03:00
tags: []
keywords: []
weight: 100
toc: true
draft: false
---

The following principles inform everything that I do as a technical communicator.
That is not an exaggeration: whenever I make a tough decision, I try to choose the action that maximizes the qualities demanded by these guidelines.
From these principles, I can also justify the [common practices]({{< relref "practices" >}}) that I exhaustively list in the next page.

## Be accurate {#truth}

Try to tell the truth.
Technical communication aims to convey some _truth_ about a technical topic.
Inaccuracies are often much worse than omissions or poorly written sentences.

However, some imprecisions are necessary shortcuts.
Besides so-called [lies to children](https://en.wikipedia.org/wiki/Lie-to-children)), almost all technical communication involves some model or abstraction.

Choosing the "correct" quantity of truth requires much judgment and authorial understanding.
This leads to the second most important principle: write for your audience.

## Understand your information's context, content, and readers {#context}

This is a fancy way of saying that you must write for your audience.
* What is the goal of the text?
* Who is going to read it?
* Why will they read it?
* How will they find?

With answers to these questions&mdash;even very general answers&mdash;you'll have a much easier time selecting the relevant content and presenting this info in a useful design.
The world is full of information. Make your signal strong.

For further reading, the book _Information Architecture for the Web and Beyond_ discusses strategies to balance these considerations at great length.

## Be as short and simple as possible {#short}

Maximize the signal-to-noise ratio.
Use the least amount of text, the most meaningful examples, and the simplest vocabulary.

All ambiguity makes your text vulnerable to unintended readings.
Long text is more likely to be misunderstood or unread.
Unnecessary jargon or excessive technical detail distract from the essential message.

But what if your topic is extremely complex&mdash;something comparable to the work of Donald Knuth? 
Here's what Knuth himself says:
[^knuth]

{{< blockquote author="Donald Knuth, from a lecture on technical writing" >}}
In general, don’t use jargon unnecessarily. Even specialists in a field get more pleasure from papers that use a
nonspecialist’s vocabulary.
{{< /blockquote >}}

[^knuth]: https://jmlr.csail.mit.edu/reviewing-papers/knuth_mathematical_writing.pdf

For those who don't know, Knuth also wrote a notoriously exhaustive, [multi-volume book on programming algorithms](https://en.wikipedia.org/wiki/The_Art_of_Computer_Programming).
He also invented some famous algorithms.
So Knuth is not only an accomplished technical writer but is also as much of a specialist as a specialist can be.
Yet even he says, "specialists get more pleasure from a nonspecialist's vocabulary."

Use the least amount of information as necessary.
What constitutes as _necessary_ depends on who your audience is.
For beginners with Unix, the concept that _everything is a file_ might provide a very helpful heuristic for thinking about their system.
For experts working in deep system internals, the same phrase may be annoyingly facile, maybe even incorrect (this exact topic has come up in flamey Linux kernel discussions[^linux]).

[^linux]: Linus Torvalds. "The UNIX philosophy is often quoted as "everything is a file", but that
really means "everything is a stream of bytes". 2007. [Linux kernel development mailing list](https://yarchive.net/comp/linux/everything_is_file.html).)


## Understand that people don't read {#no-reading}

Another reason to be concise is that people don't read much.
A study by Nielsen and Norman found that people read about 20% of a page.[^noread]
Instead, they scan documents in an [F-shaped pattern](https://www.nngroup.com/articles/f-shaped-pattern-reading-web-content-discovered/):

![F-shaped pattern](/images/f-shaped.jpg)

If you design documents to put the most important information in these hot zones, you'll support the natural way that people read text on the internet.

Also, since you can almost never count on people to read a whole text,
some redundancy is effective.
This could be redundancy of content (same info in different places) or repetition of structures.

## Be coherent {#coherent}

Coherency involves two things:
- Applying consistent terms and structures.
- Ordering information in a way that is unambiguous to follow.

Inconsistencies increase the amount of information noise, degrading reader experience.
Inconsistencies might also suggest that the text does not present a coherent picture of the technical topic, degrading the writer's worthiness as a writer of truth-telling documents.

Coherency doesn't only help readers:
consistent terms and structures also help writers re-use text and find logical flows of information to build upon.

## Write accessible documents {#accessible}

Write text for the broadest possible audience.
A good technical text should be:
- Understandable by the widest range of people (in terms of both technical and language proficiency)
- Accessible by the widest range of devices (including assistive technologies).

Even experts prefer simple explanations.
Improving accessibility also creates globally more robust designs.
For example, following standards for accessible HTML markup helps both people who use screen readers
and machines that scrape and index your pages.

## Know your audience {#audience}

Yes, I've repeated myself.
Though you want to use as little information as possible, you must also accept that people generally don't read, often forget things, and will access your document from different virtual, physical, and mental places.
To account for audience variation and to be as available to the broadest possible audience, don't be afraid to use a little repetition.

What about this text? Did you really read it all? Maybe you read only the beginning and skipped to the end.
Or maybe you really read the whole thing (thanks!). Either way, please let me repeat myself.

Always consider these questions when you write technical texts:

- Who is the primary audience?
- Who is the secondary audience?

{{< note >}}
    
People of the future are your audience too.
Many of these edits also make the work more future proof, improving writer comfort at the same time.

{{< /note >}}

## Next step: applying these principles

Without examples, these principles are only theory.
What matters is that you can apply them in the way that makes sense for your text.
For a long list of how I apply these communication principles in the specific practice of software documentation, read [Common edits in technical documentation]({{< relref "practices" >}}).

If you are looking for deeper conceptual understanding, I've listed the resources that most influenced this list in [Reading list]({{< relref "reading-list" >}}).

    
[^mayfield]: Mayfield, [Section 1.1.](http://web.mit.edu/course/21/21.guide/eff-char.htm). They use the term "accurate" instead of "correct," but I thought 4 C's is easier to remember.
[^noread]: https://www.nngroup.com/articles/how-little-do-users-read/ 
  
