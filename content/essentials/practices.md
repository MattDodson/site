---
title: |
  Recommended practices for technical documentation
description: |
  Applying the principles of good technical communication to the practice of technical documentation. Recommended practices for site and page design, sentence structure, grammar, phrasing, and formatting.A
date: 2023-03-04T18:18:06-03:00
lastmod: 2023-09-26T14:00:00-03:00
lead: |
  Applying the principles of good technical communication to the practice of technical documentation.
  Write better, guess less.
tags: []
keywords: []
weight: 200
toc: true
draft: false
---
In the preceding document, I listed the general [principles of good technical writing]({{< relref "principles" >}}).
In this document, I list how I apply these principles to edit various levels of technical documentation.

Have faith!
Once you know a technique, you'll quickly find many places where you can make a document more readable.
For example, you probably already know a few techniques to touch up this ancient doc.

<figure>
<img src="/images/scripto-continuo.jpg"
alt="The old days. No spaces or line breaks in this text.">
<figcaption>A scroll in <a href="https://en.wikipedia.org/wiki/Scriptio_continua">Scripto continua</a>, used for centuries in Greece and Rome. Wouldn't spaces and lowercase letters make this easier to read?</figcaption>
</figure>

This list begins with practices that apply to information at the broadest scope, then narrows downward.
In this narrowing process, I start from the edits that are often the most difficult and context-dependent and finish with the edits that experienced writers can apply quite mechanically.
  
For each guideline, I try to not only provide a rule but also explain _why_ these edits usually improve the text.
The justification for each edit is grounded in the principles listed in the preceding document.
I also provide examples, especially for recommendations based on some syntactical change.

Finally, I want to mention that _nothing in this list is my original idea_. So I do my best to provide quality sources.

## A very short summary

This document is long.
If you don't want to read it all, here are the highlights.

Put the most important information at the start of every section.
: This way, the widest possible audience accesses the most widely applicable details.

Create pages that are easy for readers to scan.
: People don't read, and that's okay! Help them read less.

Write short paragraphs.
: This helps people scan.

Try to write short sentences—aim for 20 words or less.
: This reduces reader effort and leaves less possibility of misinterpretation.

Use a style guide.
: This helps ensure that you can apply consistent formatting and terminology.

## Global structure

The following recommendations apply to every level of your documentation set.
When you start writing docs, you'll save future writers much time if you keep these in mind.
From an information-architecture standpoint, these global design choices also most influence the organization of your information environment (probably your documentation website).
To borrow a metaphor from Stewart Brand, they are the [shearing layers](https://en.wikipedia.org/wiki/Shearing_layers) of a building: you can't change the decisions about these elements without also changing all the stuff inside.

However, while these recommendations apply to how you design a sidebar or top-level table of contents, they also apply to individual smaller units, like paragraphs.

{{% edits/sentences "global" %}}

## Document design

The next recommendations apply to the level of the page.
Really, they're all variations on the same two recommendations:
- Make pages that are easy to scan.
- Organize text according to how related the information is.

{{% edits/sentences "page" %}}

## Paragraphs

Generally, paragraphs in software documentation should be contained to one idea and written in a way that's easy to scan.
Sometimes, I'm tempted to over-correct and make all paragraphs only one or two sentences.
This isn't a good idea either: if you don't group sentences into paragraphs, your readers will be forced to find the inter-sentence relationships for you.

{{% edits/sentences "paragraphs" %}}

## Sentences (and phrases and words) {#sentences}

{{< note >}}

This section is more technical than the others.
But if you take the time to train your level of language awareness, you'll find that you can apply most of these edits quite mechanically.

{{< /note >}}

Sentences are the molecules of a text.
True, they have smaller particles: letters, words, clauses, and phrases.
But the sentence is the basic moveable unit of a text.

If you have an unclear, incoherent, or inaccurate sentence, your text fails on the elementary level.
The most well-structured text is still unreadable if it has unreadable sentences.
For this, even though creating a useful structure is the _hard part_,
I'd say that readable sentences are the most important part of a technical text.

Structure is critical too, as is formatting.
But, I'm going to claim that a high attention to sentences naturally imparts a higher attention to structure.
This is the only uncited claim in this long, long, guide.



{{% edits/sentences "sentences" %}}
 
## Formatting

Compared to good sentences and useful structure,
formatting is less important.
But consistent formatting highlights the structure of information.

Only one formatting rule is very important. **Use a style guide**
  
{{% edits/sentences "formatting" %}}

#### Examples of list types

{{< expandable label="List examples" level="2" >}}

{{< code >}}
Three countries have names that start with the letter "J":
  * Jamaica
  * Japan
  * Jordan
{{< /code >}}

{{< code >}}
To eat food:
  1. Open mouth.
  2. Put food in mouth.
  3. Chew.
  4. Swallow.
{{< /code >}}

{{< code >}}
There are three ways to get from point A to Point B.
 a.  On foot
 b.  By horse
 c.  By car
{{< /code >}}

{{< code >}}
The acronym CRUD combines four different terms:
Create:
  To add a record
Read:
  To get a record
Update
  To change a record
Delete
  To remove a record
{{< /code >}}

{{< /expandable >}}

## Conclusion

Every recommendation on this list goes back to a core principle.
No recommendation on this list is new; some ideas are probably thousands of years old.

It's also worth noting that most proposed edits have multiple reasons,
and sometimes one reason overrides another.
Technical writing is a subset of information management, and information management is a holistic discipline.
Some edits support each other; other edits balance different needs.
Everything connects. Good luck!

## Acknowledgments

I'd like to thank [Alia Michaels](http://www.aliamm.com/) and [Ian Maddaus](https://github.com/IanMadd)
for their great feedback on early versions of this doc.

Of course, I'm also indebted to all the written works that I cite in the next section.
  
## Works cited

{{< expandable label="Works cited in this doc" >}}

- Asplund, Jan-Erik. “BLUF: The Military Standard That Can Make Your Writing More Powerful.” Animalz, September 9, 2019. https://www.animalz.co/blog/bottom-line-up-front/.
- Baker, Mark. Every Page Is Page One. XML Press, 2013.
- Beck, Daniel D. “Stop Excluding Your Audience.” ddbeck.com, January 3, 2013. https://ddbeck.com/stop-excluding-your-audience/.
- Bradner, Scott O. “Key Words for Use in RFCs to Indicate Requirement Levels.” Request for Comments. Internet Engineering Task Force, March 1997. https://doi.org/10.17487/RFC2119.
- Carey, Michelle, Moira McFadden Lanyi, Deirdre Longo, Eric Radzinski, Shannon Rouiller, and Elizabeth Wilde. Developing Quality Technical Information: A Handbook for Writers and Editors. IBM Press, 2014.
- ffeathers. “Death of the Gerund in Technical Documentation,” October 12, 2013. https://ffeathers.wordpress.com/2013/10/12/death-of-the-gerund-in-technical-documentation/.
- ThoughtCo. “Definiton and Examples of Faulty Pronoun Reference.” Accessed November 21, 2022. https://www.thoughtco.com/faulty-pronoun-reference-4103463.
- “Documentation Principles — Write the Docs.” Accessed November 21, 2022. https://www.writethedocs.org/guide/writing/docs-principles/#arid.
- Esslin, Martin. The Theatre of the Absurd. Anchor books, 1961.
- Experience, World Leaders in Research-Based User. “F-Shaped Pattern For Reading Web Content (Original Eyetracking Research).” Nielsen Norman Group. Accessed November 24, 2022. https://www.nngroup.com/articles/f-shaped-pattern-reading-web-content-discovered/.
- ———. “How Little Do Users Read?” Nielsen Norman Group. Accessed March 12, 2022. https://www.nngroup.com/articles/how-little-do-users-read/.
- ———. “Maintain Consistency and Adhere to Standards (Usability Heuristic #4).” Nielsen Norman Group. Accessed March 18, 2023. https://www.nngroup.com/articles/consistency-and-standards/.
- ThoughtCo. “Given-Before-New Principle (Linguistics).” Accessed November 19, 2022. https://www.thoughtco.com/given-before-new-principle-linguistics-1690815.
- Wylie’s Writing Tips. “How Long Should a Sentence Be?” Accessed November 21, 2022. https://freewritingtips.wyliecomm.com/2020-03-19/.
- “John M. Maloney - Technical Editing Note: You Can Go a Long Way in the Present Tense.” Accessed November 20, 2022. http://john.maloney.org/Editing/present_tense.htm.
- Knuth, Donald E., Tracy Larrabee, Paul M. Roberts, and Paul M. Roberts. Mathematical Writing. Cambridge University Press, 1989.
- “Language Log » Negation.” Accessed November 20, 2022. https://languagelog.ldc.upenn.edu/nll/?cat=55.
- Last, Suzan. “3.3 Lists,” January 1, 2019. https://pressbooks.bccampus.ca/technicalwriting/chapter/lists/.
- Mensh, Brett, and Konrad Kording. “Ten Simple Rules for Structuring Papers,” n.d., 12.
- “Modal Verbs.” Accessed November 21, 2022. https://www.perfect-english-grammar.com/modal-verbs.html.
- “Non-Designer’s Design Book, The: Williams, Robin: 9780133966152: Amazon.Com: Books.” Accessed November 21, 2022. https://www.amazon.com/Non-Designers-Design-Book-4th/dp/0133966151.
- “Notes on Technical Writing – Mkaz.Blog.” Accessed November 24, 2022. https://mkaz.blog/misc/notes-on-technical-writing/.
- “Novelist Cormac McCarthy Gives Writing Advice to Scientists ... and Anyone Who Wants to Write Clear, Compelling Prose | Open Culture.” Accessed November 20, 2022. https://www.openculture.com/2019/10/novelist-cormac-mccarthy-gives-writing-advice-to-scientists.html.
- pallep. “Verbs - Microsoft Style Guide.” Accessed November 24, 2022. https://learn.microsoft.com/en-us/style-guide/grammar/verbs.
- “PEP 20 – The Zen of Python | Peps.Python.Org.” Accessed November 19, 2022. https://peps.python.org/pep-0020/.
- Perelman, Leslie C., Edward Barretti, and James Paradis. The Mayfield Handbook of Technical and Scientific Writing. McGraw-Hill Higher Education, 1997.
- “Plainlanguage.Gov | Federal Plain Language Guidelines.” Accessed October 23, 2022. https://www.plainlanguage.gov/guidelines/.
- “Principles of Good Technical Writing.” Accessed March 19, 2023. http://localhost:1313/essentials/principles/.
- “Pronoun Reference,” July 8, 2014. https://www.swarthmore.edu/writing/pronoun-reference-0.
- Redish, Janice. Letting Go of the Words: Writing Web Content That Works. 1st edition. Morgan Kaufmann, 2007.
- Savage, Van, and Pamela Yeh. “Novelist Cormac McCarthy’s Tips on How to Write a Great Science Paper.” Nature 574, no. 7778 (September 26, 2019): 441–42. https://doi.org/10.1038/d41586-019-02918-5.
- ———. “Novelist Cormac McCarthy’s Tips on How to Write a Great Science Paper.” Nature 574, no. 7778 (September 26, 2019): 441–42. https://doi.org/10.1038/d41586-019-02918-5.
- “Sentence Length: Why 25 Words Is Our Limit - Inside GOV.UK.” Accessed March 12, 2022. https://insidegovuk.blog.gov.uk/2014/08/04/sentence-length-why-25-words-is-our-limit/.
- “Style Guides — Write the Docs.” Accessed January 8, 2023. https://www.writethedocs.org/guide/writing/style-guides/.
- “Technical Writing | Google Developers.” Accessed November 21, 2022. https://developers.google.com/tech-writing.
- “The Everything-Is-a-File Principle (Linus Torvalds).” Accessed November 25, 2022. https://yarchive.net/comp/linux/everything_is_file.html.
- “The Inverted Pyramid - Purdue OWL® - Purdue University.” Accessed November 21, 2022. https://owl.purdue.edu/owl/subject_specific_writing/journalism_and_journalistic_writing/the_inverted_pyramid.html.
- Tripathi, Ankita. “Documentation Templates and The Good Docs Project – Guest Post by Ankita Tripathi.” I’d Rather Be Writing, November 17, 2020. https://idratherbewriting.com/blog/documentation-templates-good-docs-project/.
- Tufte, Edward R. The Cognitive Style of PowerPoint : Pitching out Corrupts Within. Cheshire, Conn. : Graphics Press, 2006. https://www.edwardtufte.com/book/the-cognitive-style-of-powerpoint-pitching-out-corrupts-within-ebook/
- “Understanding Success Criterion 2.4.4 | Understanding WCAG 2.0.” Accessed November 20, 2022. https://www.w3.org/TR/UNDERSTANDING-WCAG20/navigation-mechanisms-refs.html.
- “Using Old / New Information Order in a Sentence.” Accessed November 19, 2022. https://www.student.unsw.edu.au/using-old-new-information-order-sentence.
- ThoughtCo. “What Is Embedding in Grammar?” Accessed January 7, 2023. https://www.thoughtco.com/embedding-grammar-1690643.
- “Why You Should Have a Program Check Your Links.” Accessed November 24, 2022. https://wellshapedwords.com/posts/linkchecking/link-check-your-site/.
- Williams, Joseph M., and Joseph Bizup. Style: Lessons in Clarity and Grace. 11th edition. Boston: Pearson, 2013.
- “Write Descriptive Headings and Labels - Access Guide.” Accessed January 8, 2023. https://www.accessguide.io/guide/descriptive-headings.

{{< /expandable >}}

