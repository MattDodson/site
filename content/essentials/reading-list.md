---
title: "Essential works on technical communication"
description: |
  Great resources on technical communication in general, or technical writing in particular.
date: 2023-01-04T18:18:06-03:00
tags: []
keywords: []
weight: 300
toc: false
draft: false
---

These works have most influenced my approach to technical communication.

It turns out that none happen to be about technical writing in particular.
For resources about technical writing and documentation, head to the beloved [WriteTheDocs](https://www.writethedocs.org) site and community,
or read about the beloved [Diataxis framework](https://diataxis.fr).

## Essential Reading

**Writing**
- _Style: Lessons in Clarity and Grace_. By Joseph Williams and Joseph Bizup.

**Technical communication**
- _Trees, Maps, and Theorems_. By Jean-Luc Doumount.

**Document Design**
- _Non-Designer's Design Book_. By Robin Williams.

**Visual communication**
- _The Visual Display of Quantitative Information_. Edward Tufte.


### Honorable mentions

**About writing software documentation**

- _Developing Quality Technical Information: A Handbook for Writers and Editors_.
  By the IBM content team.

**Information Architecture**
- _Information Architecture for the Web and Beyond_.

  About the variety of subjects that fall into the discipline of information architecture.

## Great examples of technical writing

- [_The Awk Programming Language_](https://archive.org/details/pdfy-MgN0H1joIoDVoIC7). By A, W, and K.

  Fantastic. The first chapter starts with an overview of useful one liners,
  the next describes the entire capabilities of the application, and the subsequent chapters provide detailed guides for increasingly esoteric applications of Awk, starting with formatting data for reports and finishing with making assemblers and interpreters.
- [Blog and zines](https://jvns.ca/). By Julia Evans.

  Julia Evans' work&mdash;especially the zines&mdash;is a fantastic example of inclusive, respectful technical communication.
  All of these topics are complex, but, since she writes in such a clear, fun, and joyful way, the effect is inviting.
  As a non-expert, I feel encouraged to try and understand things that I previously believed were prohibitively difficult.

- [_Practical Typography_](https://practicaltypography.com/). By Matthew Butterick.

  This book made me excited to learn about typography.

- [Finishing a tube of chapstick](https://web.archive.org/web/20230319235730/https://old.reddit.com/r/AskReddit/comments/4zyquo/those_who_have_finished_a_chapstick_or_lip_balm/d700cdd/) Reddit comment.

  The eye for detail, the precise vocabulary, and the rhythm and structure of the sentences make this comment not only informative but even beautiful.

**What about docs?**

I hesitate to cite specific documentation pages, because they can be as ephemeral as applications.
What's more, if I'm not using the docs, I can't comment on how well they serve their readers.
In the times when I have needed to develop something, I've especially liked the docs for the Mozilla Developer Network, Postgres, OpenAPI, Digital Ocean, RevealJS, Emacs, and k6 (I'm biased towards the last one).
