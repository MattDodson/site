---
title: "How this site was built"
description: "How I built this site, and a log of various changes and issues."
lead: 
date: 2021-12-31T17:05:44-03:00
tags: []
toc: true
draft: false
---
This site is a place where I can experiment without wasting someone else's time or money.
Besides this build summary, I also keep a [log of issues and ideas](./problems).
That log really is documentation for me, but it might interest others.

If you find a security flaw, [I might pay you some money](./bug-bounty).

## Build summary

I use Hugo to build the site, with a [modified verson of the Cupper theme](https://github.com/zwbetz-gh/cupper-hugo-theme).
I chose Hugo because I'm familiar with it, and because I'm starting to appreciate its power.
I chose Cupper because it's an accessible theme that I thought looked nice.
This theme also seemed simple enough to customize.

[The code is hosted on GitLab](https://gitlab.com/MattDodson/site).
All the work I've done has been on Github, so I wanted to try something different.
I use GitLab's CI to deploy the site to a Debian box, hosted on Digital Ocean.
Nginx serves the pages.

The logo was made using [Caro Asercion's paper crane icon](https://web.archive.org/web/20230922205512/https://game-icons.net/1x1/caro-asercion/paper-crane.html).
I edited the SVG using Inkscape.
