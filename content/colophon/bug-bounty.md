---
title: "Bug bounty"
description: "What other portfolio site has a bug bounty?"
lead: >-
  I'm a noob, and I'm figuring things out as I go.

  If I've made any silly mistakes, I'd love to know.
date: 2022-01-08T14:37:24-03:00
tags: []
toc: false
draft: false
---

If you can find some valid exploit, I'll pay you $50 on Paypal.
I'll also write about the bug and the fix, giving you credit (if you want).

Here are the ways you can pwn me:
* Take control of the site in some way; e.g. have the ability to edit files, or execute code, etc.
* Expose some personal information that's not obviously available on the site.
I'm talking about SSH keys and credit card numbers, not what high school I went to.

If you found something, send an email. `{{< email >}}`.
