---
title: "Issues and build log"
description: "This site might be simple, but it's not simple for me!"
lead: >-
  For how simple this site is, I've run into many problems.
  And I found many solutions through pure luck:
  finding a post by someone who already had the problem.
   </b>
   I want to document the problems I've had.
date: 2022-01-10T19:33:23-03:00
tags: ["hugo", "nginx", "linux", "graphics"]
alias:
  - "/colophon/problems-making-this-site/"
toc: true
draft: false
---

Really, I am this article's primary audience.
But if it helps some other lucky searcher, that's great.

## Issues

Things to do in the mythical _soon_.

- `TODO`: discover how to program redirects in nginx
  - `TODO`: Flatten all blog posts
- `TODO`: convert this doc to org-mode (for fun)
- `TODO`: Add git diff css class with colors
- `TODO`: Make list pages have a little page preview, perhaps with:
  - The page's `description` text
  - A list of post tags
  - The number of posts inside
  - A word count
- `TODO`: Add little svg icons for my GitHub and Gitlab accounts in the sidebar

## Build log

This list serves mostly to document the problems I've run into.

### December 2024

Using MathJax CHTML output had terrible amounts of buffer above and below each LaTeX symbol, making the formulas unreadable. 

### February 2024
- **CI:**

  The linkcheck CI job started timing out (after checking all links), so I thought at least to sequence the build and link check jobs.

  The Gitlab stages fails with this perplexing error:

   ![Can't run build_site, only thing you can run is build_site](/images/build-log/gitlab-unable-to-build-site.png)

 The **Validate** feature in pipeline editor helped me find the fix. The job needs an explicit `stage` property:

  ```yaml
  build_site:
    stage: build_site
    timeout: 5 minutes
  ```

  I didn't like that redundancy so I used the [default stage](https://docs.gitlab.com/ee/ci/yaml/#stages) keyword `.pre` to run the `build_site` job first.
  This fixed the sequence error, but it added a new error: the linkinator now checked a `public` directory that I guess was deleted as soon as the `build_site`job finished. So I changed the job to linkcheck my site directly. It is still timing out after 5 minutes, though.

### July 2023
- Debian
  - I wanted to upgrade my system from Debian Bullseye to Bookworm.
  Apt reported all packages were up-to-date, but the versions were clearly old.

    The problem was that I was still using the Bullseye sources. I needed to change `/etc/apt/sources.list` to something like this:

    ```
    deb http://deb.debian.org/debian bookworm main
    deb-src http://deb.debian.org/debian bookworm main

    deb http://deb.debian.org/debian-security/ bookworm-security main
    deb-src http://deb.debian.org/debian-security/ bookworm-security main

    deb http://deb.debian.org/debian bookworm-updates main
    deb-src http://deb.debian.org/debian bookworm-updates main
    ```

    Alternatively, I could just use the release class name, `stable`, but the Debian docs say:

    {{< blockquote >}}
Avoid using stable in your sources.list as that results in nasty surprises and broken systems when the next release is made; upgrading to a new release should be a deliberate, careful action and editing a file once every two years is not a burden.
     {{< /blockquote >}}

     Related docs: [SourcesList](https://wiki.debian.org/SourcesList) wiki

- CI
  - The OG image has two URLs, the `secure` url with a relative path, and the unsecure (?) URL that's hardcoded to the domain. The linkchecker runs before the site is deployed so it correctly flags the `og:image` path as not found, even though the image does exist in the repository.

    Not sure about fixes: maybe separate images to another place, maybe remove the hardcoded URL, maybe change the linkchecking flow.

### June 2023

- With [`869629`](https://gitlab.com/MattDodson/site/-/commit/8696294f494ef170916936e47d02b15963d2a812), I added open graph metadata. Then I tested it with an SVG and the graph didn't display.
  It turns out that Open Graph doesn't support SVG. Hugo can automatically convert images, so I was optimistic about adding auto-processing to my partial, but it turns out [Hugo can't recognize SVG either](https://discourse.gohugo.io/t/ho-do-i-convert-a-generic-resource-to-image-resource/22570). So for this kind of media, raster is the only way to go.

### April 2023

- RSS: Didn't set `baseURL` in config, which caused RSS links to be incorrect.
- Certain "thumbs-up" icons don't work in Safari browsers. Need to use SVGs here.

### September 2022

* CSS
  * When I checked my link-check benchmark post on mobile, I noticed that the table is overflowing.
  I found a [related GitHub issue](https://github.com/zwbetz-gh/cupper-hugo-theme/issues/6),
  and it turns out making tables unscrollable is by design.
  Now I think that instead of adding horizontal scroll, I should present data in a way that works on all devices.

  <figure>
  <img src="/images/build-log/table-overflow.png" alt="the text in the table cells are overflowing">
  <figcaption>I should at least transpose this table. Maybe I should consider not using tables at all. I regret all the horizontal scroll I've brought into the world.</figcaption>
  </figure >



### February 2022

* Git
  * I renamed the link-checking subpath (ironic!).
    When I did, I saw that the old path remained in my site's index.
    I couldn't understand how to fix it. I even went into the server and manually deleted it.
    Then, when I committed, it happened again!
    At last I remembered&mdash;I had stashed these changes.

* Hugo
  * I wanted to add a little command to a post I'd already published.
    So I used the `cmd` shortcode that comes with the theme.

    ```shell
    {{</* cmd */>}}
    echo "what's wrong here?"
    {{</* cmd */>}}
    ```
   * Experienced eyes may have already seen the issue:
     I forgot to close the shortcode tag!
     The closing tag should have been like this: `/cmd`.
   * But I pushed and didn't check.
     I didn't realize that I had formatted the rest of the article as a command until the next day.
     Always test how it looks locally! Even small changes.
   * Amazingly, as I typed this example, I reproduced the issue!
     The shortcode executed in the codefence, so I had to figure out how to escape a shortcode.
     [This wasn't documented in 2015](https://discourse.gohugo.io/t/a-way-to-mark-plain-text-and-stop-hugo-from-interpreting/1325/4)
     and it might still not be documented, because I had to figure out how to do it by looking at an old thread from 2015.
   * Fortunately I tested before I deployed this time.
   * Org mode support works, until you want to have a multi-paragraph list item.

### January 2022

In this month, I built and deployed the site.

I'd never used Nginx before. But Digital Ocean's docs about setting up a site are very good. These server problems were easiest to fix.

The SVG issues were probably the most confusing to me.

Hugo is Hugo.
It's never been intuitive to me, but I always seem to brute force my way into doing what I want to do.

* Certbot
   * For my Digital Ocean domains, I added only `wellshapedwords.com`.
     So, on my first attempt couldn't get a certificate for `www.wellshapedwords.com`.

* GitLab CI
  * See Git submodules
  * Couldn't use my key as a protected variable until I made the main branch "protected"
  * I was having trouble passing Gitlab variables to Rsync in this format `"$USER"@"$HOST":"$SITE_PATH"`.
  I tried without quotes too.
  I don't think it matters to hide these variables, but it seems better to me to do so.
  Finally I just made `$user@host` as one variable and said be done with it.
  But I'm still not sure what combination of characters and escapes I should have used.

* Git submodules
  * Editing CSS, I "Detached my head." Although it was working locally, GitLab
  CI wouldn't build the site. Finally wasted a lot of time trying to delete submodules,
  removing CI runner caches, and re-adding the submodule.
  * This undid all the CSS changes I made.
    So, I've set my own fork as the Git submodule.
    That required some trial and error with the files in `.git/modules`, `.gitmodules` and `themes`.
    Hopefully it'll be easier to manage now.

* Hugo
  * Apparently I have to change the CSS in the `assets` directory of my git sub-module folder.
  * Therefore, I'm still not sure what the point of the CSS file in `static/css` is.
  * Without setting my config `baseURL` to `/`, the public site was looking for fonts in `/css/css`.
  Fortunately, the same week I had the problem, [someone made an issue about this](https://github.com/zwbetz-gh/cupper-hugo-theme/issues/67#issue-1094874319).

* Linux
  * The logo wasn't showing on my served site. Simple fix: give the file read permissions.
* Logo
  * The first time I made the logo, I corrupted the SVG file. For some reason it still served in Firefox, but not Chromium.
  * I opened the file and saw a bunch of gobbledygook─like any binary file.
    I thought that was normal because I thought SVGs were binary files.
  * Then I looked on the internet and saw people talking about using text editors to edit SVG files.
  * This is how I learned that SVG is XML-based.

* Nginx
  * When I added the domain name to my `/etc/nginx/sites-available/<domain-name>`,
  I didn't include the `.com`. When I fixed it, I also changed the directory names to have `.com` too.
  * This created a second problem with dead symlinks.
